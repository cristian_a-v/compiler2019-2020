%{
//stack values
typedef union
{
	int i;
	char* s;
} tipst;

#define YYSTYPE tipst

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "y.tab.h"

extern YYSTYPE yylval;

void init(char *nfin,char *nfout,FILE **fisin,FILE **fisout)
{
	char numef[100];
	if (nfin) strcpy(numef,nfin);
	else
	{
		printf("Dati numele fisierului de intrare: ");
		gets(numef);
	}
	yyin=fopen(numef,"r");
	*fisin=yyin;
	if (!yyin)
	{
		printf("Nu am putut deschide fisierul de intrare.");
		exit(1);
	}
	if (nfout) strcpy(numef,nfout);
	else
	{
		printf("Dati numele fisierului de iesire:  ");
		gets(numef);
	}
	yyout=fopen(numef,"w");
	*fisout=yyout;
	if (!yyout)
	{
		printf("Nu am putut crea fisierul de iesire.");
		exit(1);
	}
}

%}

cifra [0-9]
nrIntreg {cifra}+

%%

"else"       			{printf("ELSE \n"); return ELSE; }
"altfel"       			{printf("ELSE \n"); return ELSE; }
"if"          		   	{printf("IF \n"); return IF; }
"daca"				{printf("IF \n"); return IF; }
"=="				{printf("EQUAL \n"); return EQUAL; }
"!="				{printf("NOTEQUAL \n"); return NOTEQUAL; }
">="				{printf("GREATEROREGUAL \n"); return GREATEROREQUAL; }
"<="				{printf("LOWEROREQUAL \n"); return LOWEROREQUAL; }
"<"				{printf("LOWER \n"); return LOWER; }
">"				{printf("GREATER \n"); return GREATER; }
"||"				{printf("OR \n"); return OR; }
"&&"				{printf("AND \n"); return AND; }
"=" 				{printf("EGAL \n"); return EGAL;}
"-"				{printf("MINUS \N"); return MINUS;}
";" 				{printf("PUNCTSIVIRGULA \n"); return PUNCTSIVIRGULA;}
"+"				{printf("PLUS \n"); return PLUS; }
"/" 				{printf("IMPARTIRE \n"); return IMPARTIRE; }
"*"				{printf("INMULTIRE \n"); return INMULTIRE; }
"("				{printf("STARTBRACE \n"); return STARTBRACE; }
")" 				{printf("ENDBRACE \n"); return ENDBRACE; }
"PRINT"				{printf("PRINT \n"); return PRINT; }
"{"				{printf("STARTCURLYBRACE \n"); return STARTCURLYBRACE; }
"}"				{printf("ENDCURLYBRACE \n"); return ENDCURLYBRACE; }
\"([^\\\"]|\\.)*\"		{printf("STRING \n"); yylval.s=(char *)malloc(sizeof(char)*(strlen(yytext)+1)); strcpy(yylval.s,yytext); return STRING;}
{nrIntreg}			{printf("NUMAR\n"); yylval.i=atoi(yytext); return NUMAR; } 
[a-zA-Z]|[0-9a-zA-Z]+		{printf("CUVANT \n"); yylval.s=(char *)malloc(sizeof(char)*(strlen(yytext)+1)); strcpy(yylval.s,yytext); return CUVANT;}
[\n]                		{}
[ ]+             		{}
[\t]                		{}



%%

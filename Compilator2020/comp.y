%{
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

typedef union
{
	int i;
	char* s;
} tipst;

#define YYSTYPE tipst				/* tipul valorilor din stiva yacc */

int linie=1,nvar=1,nvaraux=0,varaux[32];
int values[32];
char denvar[32][20];
FILE *fisin,*fisout;

void yyerror(char *s)
{
	printf("Eroare linia %d: %s\n",linie,s);
	exit(1);
}

int varauxnoua()
{
	int v;
	if (nvar+nvaraux>=32) { yyerror("Nu mai exista registrii liberi."); }
	v=31;
	while (v>0)
	{
		if (!varaux[v]) break;
		v--;
	}
	varaux[v]=1; nvaraux++;
	return v;
}

int nrRegistru(const char *registru)
{
char* aux = (char*)malloc(20);
strcpy(aux, registru);
if(aux[0] == '$')
strcpy(aux, aux+1);
return atoi(aux);

}

void scoatevaraux(int v)
{
	values[v] = 0;
	varaux[v]=0;
}

int variabila(char *s)
{
	int i;
	for (i=0;i<nvar;i++) if (!strcmp(s,denvar[i])) return i;
	if (nvar+nvaraux>=32) { yyerror("Nu mai exista registrii liberi."); }
	strcpy(denvar[nvar++],s);
	return nvar-1;
}

%}

%token NUMAR CUVANT EGAL PUNCTSIVIRGULA SLASH STARTBRACE ENDBRACE STARTCURLYBRACE ENDCURLYBRACE IF ELSE PRINT STRING
%token NOTEQUAL EQUAL GREATER GREATEROREQUAL LOWER LOWEROREQUAL OR AND
%left PLUS MINUS					/* asociativitate stanga, precedenta mai mare */
%left IMPARTIRE INMULTIRE
%left UNARYMINUS
%start linie
%%

linie:  | linie	'\n' { linie++; printf("La line nu crapa\n"); }
	| linie instr	{ linie++; printf("La line instr nu crapa\n"); }
	;

instrIf: CUVANT EGAL expr PUNCTSIVIRGULA {
			printf("instrIF nu crapa\n");
			int v;
			if ($3.s[0]=='$') fprintf(fisout,"\tmove\t$%d,%s\n\n",variabila($1.s),$3.s);
			else fprintf(fisout,"\taddi\t$%d,$0,%s\n\n",variabila($1.s),$3.s);
			strcpy($3.s,$3.s+1);
			v=atoi($3.s);
			if (v>nvar) scoatevaraux(v);
			free($1.s); free($3.s);
			fprintf(fisout,"\tj \tEndIf\n");
		}
	;
instrIfAndElse: CUVANT EGAL expr PUNCTSIVIRGULA {
			fprintf(fisout,"Else:\n");
			printf("instrIFElse nu crapa\n");
			int v;
			if ($3.s[0]=='$') fprintf(fisout,"\tmove\t$%d,%s\n\n",variabila($1.s),$3.s);
			else fprintf(fisout,"\taddi\t$%d,$0,%s\n\n",variabila($1.s),$3.s);
			strcpy($3.s,$3.s+1);
			v=atoi($3.s);
			if (v>nvar) scoatevaraux(v);
			free($1.s); free($3.s);
			fprintf(fisout,"\tj \tEndif\n");
		}
	;
	
instrWithoutIf: CUVANT EGAL expr PUNCTSIVIRGULA	{
			printf("La CUVANT = expr; nu crapa\n");
			int v;
			if ($3.s[0]=='$') fprintf(fisout,"\tmove\t$%d,%s\n\n",variabila($1.s),$3.s);
			else fprintf(fisout,"\taddi\t$%d,$0,%s\n\n",variabila($1.s),$3.s);
			strcpy($3.s,$3.s+1);
			v=atoi($3.s);
			if (v>nvar) scoatevaraux(v);
			free($1.s); free($3.s);
		}
	;
blockInstr: instr {
			printf("BlockInstr\n");
		}
	  | instr blockInstr{
			printf("BlockInstr\n");
			fprintf(fisout,"\tj \tEndif\n");
		}
	;

instr: CUVANT EGAL expr PUNCTSIVIRGULA
		{
			printf("La CUVANT = expr; nu crapa\n");
			int v;
			if ($3.s[0]=='$'){ fprintf(fisout,"\tmove\t$%d,%s\n\n",variabila($1.s),$3.s); values[variabila($1.s)] = values[nrRegistru($3.s)];  }
			else
			{fprintf(fisout,"\taddi\t$%d,$0,%s\n\n",variabila($1.s),$3.s);
			values[variabila($1.s)] = atoi($3.s); }
			strcpy($3.s,$3.s+1);
			v=atoi($3.s);
			if (v>nvar) scoatevaraux(v);
			free($1.s); free($3.s);
		}

	|PRINT STARTBRACE expr ENDBRACE PUNCTSIVIRGULA
	{
		if($3.s[0] != '$')
		fprintf(fisout,"PRINT: %d\n", nrRegistru($3.s));
		else fprintf(fisout, "PRINT: %d\n", values[nrRegistru($3.s)]);
	}

	|PRINT STARTBRACE stringExpr ENDBRACE PUNCTSIVIRGULA
	{
	fprintf(fisout, "PRINT: %s\n", $3.s);
	}	
	
	| IF STARTBRACE boolean ENDBRACE instrWithoutIf  {printf("IF 1\n");
											fprintf(fisout,"\tj \tEndIf\n"); 
											fprintf(fisout,"EndIf:\n"); 
										}
	| IF STARTBRACE booleanElse ENDBRACE instrIf ELSE instrIfAndElse {
									printf("IF 2\n");
									fprintf(fisout,"EndIf:\n"); 
									 }
	| IF STARTBRACE boolean ENDBRACE STARTCURLYBRACE blockInstr ENDCURLYBRACE {
										printf("IF 3\n");
										fprintf(fisout,"\tj\tEndIf\n");
										fprintf(fisout,"EndIf:\n");
										}
	| IF STARTBRACE booleanElse ENDBRACE STARTCURLYBRACE blockInstr ENDCURLYBRACE ELSE instrIfAndElse {
													printf("IF 4\n");
																												fprintf(fisout,"EndIf:\n"); }
	| IF STARTBRACE booleanElse ENDBRACE instr ELSE STARTCURLYBRACE blockInstr ENDCURLYBRACE {printf("IF 5\n");
																						fprintf(fisout,"EndIf:\n"); }
																					
	| IF STARTBRACE booleanElse ENDBRACE STARTCURLYBRACE blockInstr ENDCURLYBRACE ELSE STARTCURLYBRACE blockInstr ENDCURLYBRACE {printf("IF 6\n");
																						fprintf(fisout,"EndIf:\n"); }
			

booleanElse: expr NOTEQUAL expr    {printf("booleanElse 1\n");
								fprintf(fisout,"\tbne \t%s,%s,Else\n",$1.s,$3.s);
							}
	| expr EQUAL expr          {printf("booleanElse 2\n");
								fprintf(fisout,"\tbeq \t%s,%s,Else\n",$1.s,$3.s);
							}
	| expr GREATER expr        {printf("booleanElse 3\n");
								int v;
								v=varauxnoua();
								fprintf(fisout,"\tslt \t$%d,%s,%s\n",v,$1.s,$3.s);
								fprintf(fisout,"\tbeq \t$%d,%d,Else\n",v,1); 			
							}
	| expr GREATEROREQUAL expr {printf("booleanElse 4\n");
								int v;
								v=varauxnoua();
								fprintf(fisout,"\tslt \t$%d,%s,%s\n",v,$1.s,$3.s);
								fprintf(fisout,"\tbeq \t$%d,%d,Else\n",v,1);
								fprintf(fisout,"\tbeq \t$%s,%s,Else\n",$1.s,$3.s);
							}
	| expr LOWER expr          {printf("booleanElse 5\n");
								int v;
								v=varauxnoua();
								fprintf(fisout,"\tslt \t$%d,%s,%s\n",v,$1.s,$3.s);
								fprintf(fisout,"\tbeq \t$%d,%d,Else\n",v,0); 
								
							}
	| expr LOWEROREQUAL expr   {printf("booleanElse 6\n");
								int v;
								v=varauxnoua();
								fprintf(fisout,"\tslt \t$%d,%s,%s\n",v,$1.s,$3.s);
								fprintf(fisout,"\tbeq \t$%d,%d,Else\n",v,0);
								fprintf(fisout,"\tbeq \t$%s,%s,Else\n",$1.s,$3.s);
							}
	| booleanElse AND booleanElse      {printf("booleanElse 7\n");
								
							}
	| booleanElse OR booleanElse       {printf("booleanElse 8\n");
	
	
							}
	;
	
boolean: expr NOTEQUAL expr    {printf("boolean 1\n");
								fprintf(fisout,"\tbne \t%s,%s,EndIf\n",$1.s,$3.s);
							}
	| expr EQUAL expr          {printf("boolean 2\n");
								fprintf(fisout,"\tbeq \t%s,%s,EndIf\n",$1.s,$3.s);
							}
	| expr GREATER expr        {printf("boolean 3\n");
								int v;
								v=varauxnoua();
								fprintf(fisout,"\tslt \t$%d,%s,%s\n",v,$1.s,$3.s);
								fprintf(fisout,"\tbeq \t$%d,%d,EndIf\n",v,1); 
							}
	| expr GREATEROREQUAL expr {printf("boolean 4\n");
								int v;
								v=varauxnoua();
								fprintf(fisout,"\tslt \t$%d,%s,%s\n",v,$1.s,$3.s);
								fprintf(fisout,"\tbeq \t$%d,%d,EndIf\n",v,1); 
								fprintf(fisout,"\tbeq \t$%s,%s,EndIf\n",$1.s,$3.s);
							}
	| expr LOWER expr          {printf("boolean 5\n");
								int v;
								v=varauxnoua();
								fprintf(fisout,"\tslt \t$%d,%s,%s\n",v,$1.s,$3.s);
								fprintf(fisout,"\tbeq \t$%d,%d,EndIf\n",v,0); 
								
							}
	| expr LOWEROREQUAL expr   {printf("boolean 6\n");
								int v;
								v=varauxnoua();
								fprintf(fisout,"\tslt \t$%d,%s,%s\n",v,$1.s,$3.s);
								fprintf(fisout,"\tbeq \t$%d,%d,EndIf\n",v,0);
								fprintf(fisout,"\tbeq \t$%s,%s,EndIf\n",$1.s,$3.s);
							}
	| boolean AND boolean      {printf("boolean 7\n");
	
							}
	| boolean OR boolean       {printf("boolean 8\n");
	
							}
	;				

stringExpr:	STRING 		{ $$.s=(char *)malloc(40); sprintf($$.s,"%s",$1.s); free($1.s); printf("La stringExpr nu crapa\n");};
		| stringExpr PLUS stringExpr {
						strcpy($3.s,$3.s+1); strcpy($1.s+strlen($1.s)-1,$1.s+strlen($1.s)); $$.s = strcat($1.s,$3.s); printf("\n%s\n",$$.s);}
		| stringExpr PLUS expr { if($3.s[0]!='$') {  strcpy($1.s+strlen($1.s)-1,$1.s+strlen($1.s)); $$.s = strcat($1.s,$3.s); $$.s = strcat($$.s,"\"");}
else { strcpy($1.s+strlen($1.s)-1,$1.s+strlen($1.s)); char value[10]; sprintf(value,"%d",values[nrRegistru($3.s)]); $$.s = strcat($1.s,value); $$.s = strcat($$.s,"\"");  } }
	
			
expr:     NUMAR		{ printf("La expr : NUMAR nu crapa\n"); $$.s=(char *)malloc(20); sprintf($$.s,"%d",$1.i);}
	| CUVANT	{ $$.s=(char *)malloc(20); sprintf($$.s,"$%d",variabila($1.s)); free($1.s); printf("La expr : CUVANT nu crapa"); }
	| expr PLUS expr	{
				printf("La expr : expr PLUS expr nu crapa\n");
				int v;
				if ($1.s[0]=='$' || $3.s[0]=='$') v=varauxnoua();
 				$$.s=(char *)malloc(20);
				if ($1.s[0]=='$' && $3.s[0]!='$') {fprintf(fisout,"\taddi\t$%d,%s,%s\n",v,$1.s,$3.s); values[v] = values[nrRegistru($1.s)] + atoi($3.s); printf("\n%d\n",values[v]);}
				if ($1.s[0]=='$' && $3.s[0]=='$') { fprintf(fisout,"\tadd \t$%d,%s,%s\n",v,$1.s,$3.s); values[v] = values[nrRegistru($1.s)] + values[nrRegistru($3.s)];  }
				if ($1.s[0]!='$' && $3.s[0]=='$') {fprintf(fisout,"\taddi\t$%d,%s,%s\n",v,$3.s,$1.s); values[v] = values[nrRegistru($3.s)] + atoi($1.s);}
				if ($1.s[0]!='$' && $3.s[0]!='$') sprintf($$.s,"%d",atoi($1.s)+atoi($3.s));
				else sprintf($$.s,"$%d",v);
				if ($1.s[0]=='$')
				{
					strcpy($1.s,$1.s+1);
					v=atoi($1.s);
					if (v>nvar) scoatevaraux(v);
				}
				if ($3.s[0]=='$')
				{
					strcpy($3.s,$3.s+1);
					v=atoi($3.s);
					if (v>nvar) scoatevaraux(v);
				}
				free($1.s); free($3.s);
			}
	| expr MINUS expr	{
				printf("La expr : expr MINUS expr nu crapa\n");
				int v,v2;
				if ($1.s[0]=='$' || $3.s[0]=='$') v=varauxnoua();
 				$$.s=(char *)malloc(20);
				if ($1.s[0]=='$' && $3.s[0]!='$'){ fprintf(fisout,"\taddi\t$%d,%s,-%s\n",v,$1.s,$3.s); values[v] = values[nrRegistru($1.s)] - atoi($3.s); printf("\n%d\n",values[v]); }
				if ($1.s[0]=='$' && $3.s[0]=='$'){ fprintf(fisout,"\tsub \t$%d,%s,%s\n",v,$1.s,$3.s); values[v] = values[nrRegistru($1.s)] - values[nrRegistru($3.s)]; printf("\n%d\n",values[v]); }
				if ($1.s[0]!='$' && $3.s[0]=='$')
				{
					v2=varauxnoua();
					fprintf(fisout,"\taddi\t$%d,$0,%s\n",v2,$1.s);
					values[v2] = atoi($1.s);
					fprintf(fisout,"\tsub\t$%d,$%d,%s\n",v,v2,$3.s);
					values[v] = values[v2] - values[nrRegistru($3.s)];
					scoatevaraux(v2);
				}
				if ($1.s[0]!='$' && $3.s[0]!='$') sprintf($$.s,"%d",atoi($1.s)-atoi($3.s));
				else sprintf($$.s,"$%d",v);
				if ($1.s[0]=='$')
				{
					strcpy($1.s,$1.s+1);
					v=atoi($1.s);
					if (v>nvar) scoatevaraux(v);
				}
				if ($3.s[0]=='$')
				{
					strcpy($3.s,$3.s+1);
					v=atoi($3.s);
					if (v>nvar) scoatevaraux(v);
				}
				free($1.s); free($3.s);
			}
	| expr INMULTIRE expr	{
				printf("La expr : expr INMULTIRE expr nu crapa\n");
				int v;
				if ($1.s[0]=='$' || $3.s[0]=='$') v=varauxnoua();
 				$$.s=(char *)malloc(20);
				if ($1.s[0]=='$' && $3.s[0]!='$') { fprintf(fisout,"\tmuli\t$%d,%s,%s\n",v,$1.s,$3.s); values[v] = values[nrRegistru($1.s)] * atoi($3.s); }
				if ($1.s[0]=='$' && $3.s[0]=='$') { fprintf(fisout,"\tmult\t$%d,%s,%s\n",v,$1.s,$3.s); values[v] = values[nrRegistru($1.s)] * values[nrRegistru($3.s)]; }
				if ($1.s[0]!='$' && $3.s[0]=='$') { fprintf(fisout,"\tmuli\t$%d,%s,%s\n",v,$3.s,$1.s); values[v] = values[nrRegistru($1.s)] * atoi($3.s); }
				if ($1.s[0]!='$' && $3.s[0]!='$') sprintf($$.s,"%d",atoi($1.s)*atoi($3.s));
				else sprintf($$.s,"$%d",v);
				if ($1.s[0]=='$')
				{
					strcpy($1.s,$1.s+1);
					v=atoi($1.s);
					if (v>nvar) scoatevaraux(v);
				}
				if ($3.s[0]=='$')
				{
					strcpy($3.s,$3.s+1);
					v=atoi($3.s);
					if (v>nvar) scoatevaraux(v);
				}
				free($1.s); free($3.s);
			}
	| expr IMPARTIRE expr	{
				printf("La expr : expr INMULTIRE expr nu crapa\n");
				int v;
				if ($1.s[0]=='$' || $3.s[0]=='$') v=varauxnoua();
 				$$.s=(char *)malloc(20);
				if ($1.s[0]=='$' && $3.s[0]!='$'){ fprintf(fisout,"\tdivi\t$%d,%s,%s\n",v,$1.s,$3.s); values[v] = values[nrRegistru($1.s)] / atoi($3.s);  }
				if ($1.s[0]=='$' && $3.s[0]=='$'){ fprintf(fisout,"\tdiv\t$%d,%s,%s\n",v,$1.s,$3.s); values[v] = values[nrRegistru($1.s)] / values[nrRegistru($3.s)]; }
				if ($1.s[0]!='$' && $3.s[0]=='$'){ fprintf(fisout,"\tdivi\t$%d,%s,%s\n",v,$3.s,$1.s); values[v] = atoi($1.s) / values[nrRegistru($3.s)]; }
				if ($1.s[0]!='$' && $3.s[0]!='$') sprintf($$.s,"%d",atoi($1.s)/atoi($3.s));
				else sprintf($$.s,"$%d",v);
				if ($1.s[0]=='$')
				{
					strcpy($1.s,$1.s+1);
					v=atoi($1.s);
					if (v>nvar) scoatevaraux(v);
				}
				if ($3.s[0]=='$')
				{
					strcpy($3.s,$3.s+1);
					v=atoi($3.s);
					if (v>nvar) scoatevaraux(v);
				}
				free($1.s); free($3.s);
			}
	| MINUS expr %prec UNARYMINUS
			{
				printf("La expr : MINUS expr nu crapa\n");
				int v=varauxnoua();
 				$$.s=(char *)malloc(20);
				if ($2.s[0]=='$')
				{
					fprintf(fisout,"\tsub\t$%d,$0,%s\n",v,$2.s);
					sprintf($$.s,"$%d",v);
					strcpy($2.s,$2.s+1);
					v=atoi($2.s);
					if (v>nvar) scoatevaraux(v);
				}
				else sprintf($$.s,"%d",-atoi($2.s));
				free($2.s);
			}
	| STARTBRACE expr ENDBRACE	{ printf("La expr: STARTBRACE expr ENDBRACE\n"); $$.s=$2.s; }
	;

%%
int main(int narg,char *argv[])
{
//printf("Enter Arithmatic expr\n");
int i;
	if (narg!=3) init(NULL,NULL,&fisin,&fisout);
	else init(argv[1],argv[2],&fisin,&fisout);
	for (i=0;i<32;i++) varaux[i]=0;
	yyparse();
//yyparse();
return 0;
}

